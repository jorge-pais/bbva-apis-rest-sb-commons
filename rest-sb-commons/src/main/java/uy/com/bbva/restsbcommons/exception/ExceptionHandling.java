package uy.com.bbva.restsbcommons.exception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import uy.com.bbva.services.commons.exceptions.LoginException;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import uy.com.bbva.services.commons.utils.LogUtils;
@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class ExceptionHandling extends ResponseEntityExceptionHandler {
    @Autowired
    private LogUtils logger;
    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<Object> serviceException(ServiceException ex, WebRequest request) {
        logger.logError(this.getClass().getName(), ex.getMessage(), ex.getInternalMessage(), ex);
        return handleExceptionInternal(ex, "Bad Request",
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(LoginException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<Object> loginException(ServiceException ex, WebRequest request) {
        logger.logError(this.getClass().getName(), ex.getMessage(), ex.getInternalMessage(), ex);
        return handleExceptionInternal(ex, "Bad Request",
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<Object> Exception(Exception ex, WebRequest request) {
        logger.logError(this.getClass().getName(), ex.getMessage(), "", ex);
        return handleExceptionInternal(ex, "Bad Request",
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}