package uy.com.bbva.restsbcommons.config;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import uy.com.bbva.restsbcommons.filters.LoggingInterceptor;
import uy.com.bbva.services.commons.config.ServicesCommonsConfiguration;
@Configuration
@EnableAutoConfiguration
@Import(ServicesCommonsConfiguration.class)
@ComponentScan(basePackages = {"uy.com.bbva.restsbcommons"})
@PropertySource(value = "classpath:rest-sb-commons.properties")
public class RestSBCommonsConfiguration extends WebMvcConfigurerAdapter {
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON);
    }
    @Bean
    public LoggingInterceptor loggingHandlerInterceptor() {
        return new LoggingInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loggingHandlerInterceptor());
    }
}