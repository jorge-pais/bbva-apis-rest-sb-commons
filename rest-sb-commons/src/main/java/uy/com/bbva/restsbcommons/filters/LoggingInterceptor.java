package uy.com.bbva.restsbcommons.filters;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import uy.com.bbva.services.commons.utils.LogUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
@Component
public class LoggingInterceptor extends HandlerInterceptorAdapter {
    private static final String REQUESTID = "REQUESTID";
    private static final String CANAL = "CANAL";
    private static final String USERID = "USERID";
    private static final String REQUESTURI = "REQUESTURI";
    private static final String METHOD = "METHOD";
    private static final String PARAMETERS = "PARAMETERS";
    private static final String START_TIME = "startTime";
    @Value("${header.requestId:}")
    private String requestIdHeader;
    @Value("${header.canalId:}")
    private String canalIdHeader;
    @Value("${header.userId}")
    private String userIdHeader;
    @Autowired
    private LogUtils logger;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            request.setAttribute(START_TIME, System.currentTimeMillis());
            if (StringUtils.isNotEmpty(requestIdHeader)) {
                String requestId = request.getHeader(requestIdHeader);
                MDC.put(REQUESTID, (requestId != null) ? requestId : "");
            }
            if (StringUtils.isNotEmpty(canalIdHeader)) {
                String canalId = request.getHeader(canalIdHeader);
                MDC.put(CANAL, (canalId != null) ? canalId : "");
            }
            if (StringUtils.isNotEmpty(userIdHeader)) {
                String userId = request.getHeader(userIdHeader);
                MDC.put(USERID, (userId != null) ? userId : "");
            }
            MDC.put(REQUESTURI, request.getRequestURI());
            String method = ((HandlerMethod) handler).getMethod().getName();
            MDC.put(METHOD, method);
            MDC.put(PARAMETERS, getParameters(request));
        }
        return true;
    }
    private String getParameters(final HttpServletRequest request) {
        String parameters = "";
        final Map<String, String[]> parameterMap = request.getParameterMap();
        for (String key : parameterMap.keySet()) {
            parameters = parameters.concat(" - " + key + ": " + request.getParameter(key));
        }
        return parameters;
    }
    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        MDC.clear();
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        if (handler instanceof HandlerMethod) {
            long startTime = (Long) request.getAttribute(START_TIME);
            long endTime = System.currentTimeMillis();
            long executeTime = endTime - startTime;
            logger.logMonitoreo(this.getClass().getName(), executeTime);
        }
        MDC.clear();
    }
}